= Awesome AsciiDoc

A curated list of software and resources that support the AsciiDoc ecosystem.

.DISCLAIMER
[IMPORTANT]
====
This resource is provided as a service to the AsciiDoc community by the AsciiDoc Working Group.
The AsciiDoc Working Group curates the entries listed on this page.
New entries may be suggested the community at large by submitting a merge request to this repository.

This page is intended to promote AsciiDoc and related technologies by identifying projects that support and benefit the AsciiDoc ecosystem.
*An entry on this page does not equal an endorsement that the software is AsciiDoc compatible as defined by the Eclipse Foundation Specification Process (EFSP) for the AsciiDoc Language.*
Software projects, namely language processors, must meet the compatiblity requirements in order to be granted that title.
====

== Explore

=== Author

Author AsciiDoc content with the help of these tools.

* https://intellij-asciidoc-plugin.ahus1.de/[IntelliJ AsciiDoc Plugin] - A plugin for the IntelliJ platform (IntelliJ IDEA, RubyMine, etc.) that provides support for the AsciiDoc language.
* https://marketplace.visualstudio.com/items?itemName=asciidoctor.asciidoctor-vscode[AsciiDoc for Visual Studio Code] - An extension for VS Code that provides live preview, syntax highlighting, and snippets for the AsciiDoc language using Asciidoctor.
* https://marketplace.eclipse.org/content/asciidoctor-editor[Asciidoctor Editor for Eclipse] - A plugin for the Eclipse IDE that adds text editor support and a preview view for AsciiDoc files.
* https://atom.io/packages/asciidoc-assistant[Atom AsciiDoc Assistant] - A package for Atom that adds support for the AsciiDoc language.
* https://github.com/asciidoctor/asciidoctor-browser-extension[Asciidoctor Browser Extension] - A browser extension for Chrome/Chromium, Firefox, Opera, and Edge that uses Asciidoctor.js to provide live HTML preview of AsciiDoc files.
* https://github.com/vim/vim/blob/master/runtime/syntax/asciidoc.vim[AsciiDoc syntax for VIM] - Syntax highlighting for AsciiDoc files is included in the Vim text editor, though a https://github.com/habamax/vim-asciidoctor#vim-plug[more comprehensive plugin] is available using vim-plug.

=== Convert

Parse and convert AsciiDoc using these language implementations.

* https://docs.asciidoctor.org/asciidoctor/latest[Asciidoctor] - The leading AsciiDoc language processor, written in Ruby. Asciidoctor maintains the definition of the AsciiDoc language until it can be taken over by the AsciiDoc Language project.
* https://github.com/asciidoctor/asciidoctorj[AsciidoctorJ] - A Java library that runs Asciidoctor on the JVM and provides a Java API.
* https://github.com/asciidoctor/asciidoctor.js[Asciidoctor.js] - A version of Asciidoctor transpiled to JavaScript that includes a porcelain JavaScript API.
* https://github.com/bytesparadise/libasciidoc[libasciidoc] - A Golang library for processing AsciiDoc.
* https://github.com/gmarpons/asciidoc-hs[asciidoc-hs] - A Haskell-based AsciiDoc parser that's designed to be used as a pandoc frontend.

AsciiDoc was originally developed as part of the https://asciidoc-py.github.io[AsciiDoc.py project].
AsciiDoc.py is now a legacy processor for an older rendition of the AsciiDoc syntax.
As such, it will not properly handle the language that is being described by AsciiDoc specification.
Unless you still rely on the AsciiDoc.py toolchain, you should choose a processor that handles the current AsciiDoc syntax.

=== Publish

Publish content from AsciiDoc using these build tools and static site generators.

* https://docs.antora.org[Antora] - A modular, multi-repository site generator designed for creating documentation sites from content composed in AsciiDoc and processed with Asciidoctor.
* https://github.com/asciidoctor/jekyll-asciidoc[Jekyll AsciiDoc] - A Jekyll plugin that converts AsciiDoc source files in your site to HTML pages using Asciidoctor.
* https://jbake.org[JBake] - A Java-based static site and blog generator that can process content written in AsciiDoc.
* https://github.com/asciidoctor/asciidoctor-maven-plugin[Asciidoctor Maven Tools] - A suite of plugins for Apache Maven that convert AsciiDoc source files to HTML and other formats using AsciidoctorJ.
* https://github.com/asciidoctor/asciidoctor-gradle-plugin[Asciidoctor Gradle Plugin] - A plugin for Gradle that converts AsciiDoc source files using AsciidoctorJ.
