'use strict'

const cssnano = require('cssnano')
const litePreset = require('cssnano-preset-lite')
const cssnanoPreset = litePreset({ discardComments: false })
const ospath = require('path')
const asciidoctor = require('@asciidoctor/core')()
const autoprefixer = require('autoprefixer')
const postcss = require('postcss')
const postcssPresetEnv = require('postcss-preset-env')
const postcssAtImport = require('postcss-import')
const DocumentModelConverter = require('./lib/document-model-converter.js')

asciidoctor.ConverterFactory.register(new DocumentModelConverter(), ['json'])

module.exports = function (eleventyConfig, rest) {
  eleventyConfig.addPassthroughCopy('pages/**/*.{jpg,png,gif,svg,webp,ico,txt,json,webmanifest,xml,woff2}')
  eleventyConfig.addPassthroughCopy('pages/_redirects')
  eleventyConfig.setBrowserSyncConfig({
    files: ['_site/**/*'],
    open: false
  })
  eleventyConfig.addExtension('css', {
    outputFileExtension: 'css',
    compileOptions: {
      cache: false
    },
    compile: async function (inputContent, inputPath) {
      const parsed = ospath.parse(inputPath)
      if (parsed.name.startsWith('_')) {
        return
      }
      const result = await postcss([
        postcssAtImport,
        autoprefixer,
        postcssPresetEnv({ browsers: 'defaults,not IE 11' }),
        cssnano({ preset: cssnanoPreset })
      ])
        .process(inputContent, { from: inputPath, to: '_site/assets/css/main.css' })
      return async () => {
        return result.css
      }
    }
  })
  eleventyConfig.addExtension('adoc', {
    compile: function (inputContent) {
      return function () {
        const registry = asciidoctor.Extensions.create()
        return asciidoctor.convert(inputContent, {
          safe: 'unsafe',
          extension_registry: registry,
          template_dirs: ospath.join(__dirname, 'templates'),
          attributes: {
            'skip-front-matter': '',
            showtitle: '',
            'syntax-highlighter': 'highlight.js',
            includedir: ospath.join(__dirname, 'pages', 'examples')
          },
          backend: 'json'
        })
      }
    }
  })
  return {
    templateFormats: ['njk', 'adoc', 'css'],
    dir: {
      output: '_site',
      input: 'pages',
      includes: '_includes',
      layouts: '_layouts',
      data: '_data'
    }
  }
}
