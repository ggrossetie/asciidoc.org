import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import { babel } from '@rollup/plugin-babel'

function wrap () {
  return {
    name: 'rollup-plugin-wrap',
    transform (content, id) {
      if (id.endsWith('highlightjs-line-numbers.js/src/highlightjs-line-numbers.js')) {
        content = `export default function init(hljs) {

  window.hljs = hljs
;${content}
}`
        return {
          code: content,
          map: { mappings: '' }
        }
      }
      return null
    }
  }
}

export default {
  input: 'pages/assets/js/main.js',
  output: {
    file: '_site/assets/js/main.bundle.js',
    format: 'iife',
    strict: false // Opal does not work in strict mode :|
  },
  plugins: [
    wrap(),
    nodeResolve({ browser: true }),
    commonjs(),
    process.env.BUILD === 'production' ? babel({ babelHelpers: 'bundled', presets: ['@babel/preset-env'] }) : '',
    process.env.BUILD === 'production' ? terser() : ''
  ]
}
