import { init as initMobileMenu } from './mobile-menu.js'
import { init as initTabSet } from './tabs-block-behavior.js'
import { init as initToggle } from './toggle-behavior.js'
import { init as initInteractiveEditor } from './interactive-editor.js'
import { init as initSyntaxHighlighting } from './syntax-highlighting.js'

initMobileMenu()
initTabSet()
initToggle()
initInteractiveEditor()
initSyntaxHighlighting()
