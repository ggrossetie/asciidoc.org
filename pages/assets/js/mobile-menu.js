export function init () {
  const navbar = document.querySelector('.header > nav > .navbar-mobile')
  const closeButton = navbar.querySelector(':scope > .close')
  const openButton = navbar.querySelector(':scope > .open')
  const menu = document.querySelector('.header > nav > .menu')

  closeButton.addEventListener('click', () => {
    menu.classList.remove('is-active')
    navbar.classList.remove('navbar-open')
  })

  openButton.addEventListener('click', () => {
    menu.classList.add('is-active')
    navbar.classList.add('navbar-open')
  })
}
